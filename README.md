# cryptopen

Simple tool for opening, and mounting `dmcrypt` secure drives in a single command.

# Usage

Example usage, to open a secure device (`/dev/sda1` in this example), name it `foobar`, and mount it at `~/foobar`.

```bash
# Open and mount the device
cryptopen open /dev/sda1 foobar ~/foobar

# Use the mounted drive
echo 'bar' > ~/foobar/foo.txt

# Unmount and close the device
cryptopen close foobar ~/foobar
```

# Building

`cryptopen` is built with [`cargo`](https://crates.io).

```bash
cargo build --release
```

# Installation

There are 2 options for installation

1. Build, then install manually.
2. Install with the [AUR package `cryptopen-git`](https://aur.archlinux.org/packages/cryptopen-git).

## Manual Build/Install

First, build `cryptsetup`.

```bash
cargo build --release
```

If you want, you can build `cryptopen`, then copy the executable file from `target/release/cryptopen` to the place of your choosing. `cryptopen` also needs `cryptsetup-open` and `cryptsetup-close` to be on your `PATH`, so those two executable scripts should be installed together along with `cryptopen` itself.

```bash
install -Dm 755 -t /usr/local/bin target/release/cryptopen bin/cryptsetup-{open,close}
```

Alternatively, you can have `cargo` install the `cryptsetup` binary to `~/.cargo/bin`, then install `cryptsetup-{open,close}` as above.

```bash
cargo install --path .
install -Dm 755 -t /usr/local/bin bin/cryptsetup-{open,close}
```

## Set File capabilities

Once cryptopen is installed, it needs to have `CAP_SYS_ADMIN` set for it so that it can mount/unmount volumes. If you installed via the AUR package, then this is done for you. If you installed manually, then you'll have to run the following.

```bash
sudo setcap CAP_SYS_ADMIN=eip /path/to/cryptopen
```

## Setup sudo to not prompt for a password on cryptsetup-{open,close}

To have sudo not prompt you for a password when running `cryptopen` (which in turn runs `cryptsetup-{open,close}`), you can add an entry akin to the following for to your `/etc/sudoers` file. Doing this will prevent you from having to enter your password twice (like in instances where your password for LUKS drive encryption is the same as your login password). These scripts force the second argument of the `cryptsetup` program, so you still would have to enter your password to do dangerous things like `cryptsetup luksFormat` and whatnot.

```
yourusername ALL=(ALL) NOPASSWD: /path/to/cryptsetup-open, /path/to/cryptsetup-close
```
