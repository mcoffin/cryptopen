use std::{
    ffi::{
        CStr,
        CString,
    },
    mem,
    path::PathBuf,
};

#[derive(Debug, thiserror::Error)]
pub enum HomeDirError {
    #[error("No such user: {0}")]
    NoSuchUser(String),
    #[error("User has no home directory")]
    NoHome,
}

fn getpwnam<'a, S: AsRef<CStr>>(username: S) -> Option<&'a libc::passwd> {
    let result = unsafe {
        libc::getpwnam(username.as_ref().as_ptr())
    };
    if result.is_null() {
        None
    } else {
        unsafe {
            Some(&*result)
        }
    }
}

trait PasswdExt {
    fn home_dir(&self) -> Option<&CStr>;
}

impl PasswdExt for libc::passwd {
    fn home_dir(&self) -> Option<&CStr> {
        Some(self.pw_dir)
            .filter(|p| !p.is_null())
            .map(|p| unsafe { CStr::from_ptr(p) })
    }
}

pub fn home_dir() -> Result<PathBuf, HomeDirError> {
    let username = whoami::username();
    let c_username = CString::new(username.as_bytes()).unwrap();
    let result = getpwnam(c_username)
        .map(Ok)
        .unwrap_or(Err(HomeDirError::NoSuchUser(username)))?;
    result.home_dir()
        .map(|s| PathBuf::from(s.to_string_lossy().as_ref()))
        .map(Ok)
        .unwrap_or(Err(HomeDirError::NoHome))
}
