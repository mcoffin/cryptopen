extern crate clap;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate thiserror;
extern crate libc;
extern crate whoami;
#[macro_use] extern crate lazy_static;
extern crate sys_mount;

mod logging;
mod util;
mod libc_ext;

use clap::Clap;
use std::{
    error::Error,
    io,
    path::{
        Path,
        PathBuf,
    },
    process::Command,
};
use util::{
    PathExt,
    RunnableCommand,
    CommandError,
    CommandExt,
    CryptoDevice,
};

#[derive(Debug, Clap)]
struct Config {
    #[clap(subcommand)]
    command: SubCommand,
}

impl RunnableCommand for Config {
    type Error = <SubCommand as RunnableCommand>::Error;

    fn run(&self) -> Result<(), Self::Error> {
        self.command.run()
    }
}

#[derive(Debug, Clap)]
enum SubCommand {
    Open(OpenConfig),
    Close(CloseConfig),
}

impl RunnableCommand for SubCommand {
    type Error = Box<dyn Error>;

    fn run(&self) -> Result<(), Box<dyn Error>> {
        match self {
            &SubCommand::Open(ref config) => config.run()?,
            &SubCommand::Close(ref config) => config.run()?,
        }
        Ok(())
    }
}

#[derive(Debug, Clap)]
struct OpenConfig {
    #[clap(long = "type", short = 't')]
    fs_type: Option<String>,
    #[clap(parse(from_os_str))]
    device: PathBuf,
    name: String,
    #[clap(parse(from_os_str))]
    mount_point: Option<PathBuf>,
}

impl OpenConfig {
    #[inline(always)]
    pub fn device(&self) -> &Path {
        self.device.as_ref()
    }

    #[inline(always)]
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    pub fn mapper_path(&self) -> PathBuf {
        let mut ret = PathBuf::from("/dev/mapper");
        ret.push(self.name());
        ret
    }

    #[inline(always)]
    pub fn mount_point(&self) -> Option<&Path> {
        self.mount_point.as_ref().map(AsRef::as_ref)
    }
}

#[derive(Debug, thiserror::Error)]
enum OpenError {
    #[error("Mount point is not within user's home directory")]
    MountPoint,
    #[error("Error running command: {0}")]
    Command(#[from] CommandError),
    #[error("Error mounting device: {0}")]
    Mount(io::Error),
}

impl RunnableCommand for OpenConfig {
    type Error = OpenError;

    fn run(&self) -> Result<(), OpenError> {
        let p = self.mapper_path();

        if !p.exists() {
            Command::new("sudo")
                .arg("cryptsetup-open")
                .arg(self.device())
                .arg(self.name())
                .status_checked()
                .map_err(CommandError::from)?;
            info!("opened {} as {}", self.device.display(), self.name());
        } else {
            warn!("device at path {} is already open!", p.display());
        }

        if let Some(mount_point) = self.mount_point() {
            let home_dir = libc_ext::home_dir()
                .map_err(|e| io::Error::new(io::ErrorKind::Other, format!("{}", &e)))
                .map_err(CommandError::from)?;
            if !mount_point.starts_with(home_dir) {
                return Err(OpenError::MountPoint);
            }

            p.mount_at(mount_point, self.fs_type.as_ref().map(AsRef::as_ref))
                .map_err(OpenError::Mount)?;
            // Command::new("mount")
            //     .arg(&p)
            //     .arg(mount_point)
            //     .status_checked()?;
            info!("mounted {} to {}", p.display(), mount_point.display());
        }
        Ok(())
    }
}

#[derive(Debug, Clap)]
struct CloseConfig {
    name: String,
    #[clap(parse(from_os_str))]
    mount_point: Option<PathBuf>,
}

impl CloseConfig {
    #[inline(always)]
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }

    #[inline(always)]
    pub fn mount_point(&self) -> Option<&Path> {
        self.mount_point.as_ref().map(AsRef::as_ref)
    }

    fn unmount(&self) -> Result<(), CloseError> {
        if let Some(mount_point) = self.mount_point() {
            if !CryptoDevice::from(self.name()).is_mounted_at(mount_point)? {
                return Err(CloseError::NotMounted);
            }
            let home_dir = libc_ext::home_dir()?;
            if !mount_point.starts_with(&home_dir) {
                return Err(CloseError::MountPoint);
            }
            sys_mount::unmount(mount_point, sys_mount::UnmountFlags::empty())?;
            info!("unmounted: {}", mount_point.display());
        }
        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
enum CloseError {
    #[error("Mount point is not in user's home directory")]
    MountPoint,
    #[error("Error getting user's home directory: {0}")]
    NoHome(#[from] libc_ext::HomeDirError),
    #[error("Device was not mounted at mount point")]
    NotMounted,
    #[error("Error running command: {0}")]
    Command(#[from] CommandError),
}

impl From<io::Error> for CloseError {
    #[inline]
    fn from(e: io::Error) -> Self {
        CloseError::Command(CommandError::from(e))
    }
}

impl RunnableCommand for CloseConfig {
    type Error = CloseError;

    fn run(&self) -> Result<(), CloseError> {
        let unmount_result = self.unmount();
        debug!("closing encrypted device: {}", self.name());
        Command::new("sudo")
            .args(&["cryptsetup-close", self.name()])
            .status_checked()?;
        info!("closed encrypted device: {}", self.name());
        unmount_result
    }
}

fn main() {
    logging::init();
    util::run_main::<Config, &str>(None);
}
