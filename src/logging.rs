use std::{
    env,
    ffi::OsStr,
    sync::Once,
};

static INIT_LOGGING: Once = Once::new();
#[cfg(not(debug_assertions))]
const DEFAULT_LOG_LEVEL: log::Level = log::Level::Info;
#[cfg(debug_assertions)]
const DEFAULT_LOG_LEVEL: log::Level = log::Level::Debug;

fn env_or_default<K, V>(key: K, value: V)
where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
{
    let key = key.as_ref();
    if env::var_os(key).filter(|s| s.len() > 0).is_none() {
        env::set_var(key, value);
    }
}

pub fn init() {
    INIT_LOGGING.call_once(|| {
        env_or_default("RUST_LOG", format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        env_logger::init();
    });
}
