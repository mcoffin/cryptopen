use clap::Clap;
use std::{
    fmt::{
        Debug,
        Display,
    },
    ffi::{
        CStr,
        CString,
        OsStr,
    },
    fs,
    io,
    path::{
        Path,
        PathBuf,
    },
    process::{
        self,
        Command,
        ExitStatus,
    },
    ptr,
};

const EXIT_FAILURE: i32 = 1;

pub trait RunnableCommand {
    type Error;

    fn run(&self) -> Result<(), Self::Error>;
}

pub fn run_or_exit<T, S>(config: &T, message: Option<S>)
where
    T: RunnableCommand,
    <T as RunnableCommand>::Error: Display,
    S: AsRef<str>,
{
    match (config.run(), message) {
        (Err(e), Some(message)) => {
            error!("{}: {}", message.as_ref(), &e);
            process::exit(EXIT_FAILURE);
        },
        (Err(e), None) => {
            error!("{}", &e);
            process::exit(EXIT_FAILURE);
        }
        (Ok(_), _) => {},
    }
}

pub fn run_main<T, S>(message: Option<S>)
where
    T: RunnableCommand + Clap + Debug,
    <T as RunnableCommand>::Error: Display,
    S: AsRef<str>,
{
    let config = T::parse();
    debug!("config: {:?}", &config);
    run_or_exit(&config, message);
}

#[derive(Debug, thiserror::Error)]
pub enum CommandError {
    #[error("Error running command: {0}")]
    Io(#[from] io::Error),
    #[error("Unexpected exit status while running command: {0}")]
    Status(ExitStatus),
}

pub trait CommandExt {
    fn status_checked(&mut self) -> Result<ExitStatus, CommandError>;
}

impl CommandExt for Command {
    fn status_checked(&mut self) -> Result<ExitStatus, CommandError> {
        self.status()
            .map_err(CommandError::from)
            .and_then(|status| if status.success() {
                Ok(status)
            } else {
                Err(CommandError::Status(status))
            })
    }
}

fn unwrapped_iter<'a, T, It>(it: It) -> impl Iterator<Item=T> + 'a
where
    It: Iterator<Item=&'a T> + 'a,
    T: Copy + 'a,
{
    it.map(|&v| v)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CryptoDevice<'a>(&'a str);

impl<'a> CryptoDevice<'a> {
    pub fn mapper_path(self) -> PathBuf {
        unwrapped_iter(["/dev/mapper", self.0].iter())
            .collect()
    }

    pub fn is_mounted_at(self, p: &Path) -> io::Result<bool> {
        use io::BufRead;
        let f = fs::OpenOptions::new()
            .read(true)
            .open("/proc/mounts")
            .map(io::BufReader::new)?;
        let mapper_path = self.mapper_path();
        for line in f.lines() {
            let line = line?;
            let mut components = line.split(" ");
            let mut check_component = move |p: &Path| -> bool {
                let p: &OsStr = p.as_ref();
                return components.next()
                    .filter(|v| {
                        let v: &OsStr = v.as_ref();
                        v == p
                    })
                    .is_some();
            };
            if check_component(mapper_path.as_ref()) && check_component(p) {
                return Ok(true);
            }
        }
        Ok(false)
    }
}

impl<'a> From<&'a str> for CryptoDevice<'a> {
    #[inline(always)]
    fn from(name: &'a str) -> Self {
        CryptoDevice(name)
    }
}

pub trait PathExt {
    fn mount_at(&self, p: &Path, fs_type: Option<&str>) -> io::Result<()>;
}

impl PathExt for Path {
    fn mount_at(&self, mount_point: &Path, fs_type: Option<&str>) -> io::Result<()> {
        use sys_mount::{
            Mount,
            MountFlags,
            SupportedFilesystems,
            FilesystemType,
        };
        let supported = SupportedFilesystems::new()?;
        let fs_type = if let Some(fs_type) = fs_type {
            FilesystemType::from(fs_type)
        } else {
            FilesystemType::from(&supported)
        };
        Mount::new(self, mount_point, fs_type, MountFlags::empty(), None)?;
        Ok(())
    }
}
